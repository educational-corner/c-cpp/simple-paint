//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by WindowsProject1.rc

#define IDS_APP_TITLE			103

#define IDR_MAINFRAME			128
#define IDD_SIMPLEPAINT_DIALOG	102
#define IDD_ABOUTBOX			103
#define IDM_ABOUT				104
#define IDM_EXIT				105
#define IDI_SIMPLEPAINT			107
#define IDI_SMALL				108
#define IDC_SIMPLEPAINT			109
#define IDC_MYICON				2
#ifndef IDC_STATIC
#define IDC_STATIC				-1
#define IDD_DIALOG  110

#define SAVE_AS_ACTION 112
#define PEN_THICKNESS_01 117
#define PEN_THICKNESS_05 118
#define PEN_THICKNESS_10 119
#define COLOR_CHANGE 120

#endif
// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS

#define _APS_NO_MFC					130
#define _APS_NEXT_RESOURCE_VALUE	129
#define _APS_NEXT_COMMAND_VALUE		32771
#define _APS_NEXT_CONTROL_VALUE		1000
#define _APS_NEXT_SYMED_VALUE		110
#endif
#endif
