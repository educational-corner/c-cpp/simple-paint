#ifndef _MAIN_WINDOW_HPP_
#define _MAIN_WINDOW_HPP_

#include "base_window.h"
#include <Commdlg.h>

#include "../src/figures/pen_figure.h"
#include "../src/figures/line_figure.h"
#include "../src/figures/circle_figure.h"
#include "../src/figures/rectangle_figure.h"

#include <unordered_map>

class MainWindow : public BaseWindow<MainWindow>
{
public:

    enum EFigureActions
    {
        eFA_PenAction,
        eFA_LineAction,
        eFA_CircleAction,
        eFA_RectangleAction
    };

public:
    PCWSTR  ClassName() const { return L"Main Window Class"; }
    LRESULT HandleMessage( UINT uMsg, WPARAM wParam, LPARAM lParam );

    void saveImage();

    void chColor();

    void mouseMove( WPARAM& wParam, LPARAM& lParam );
    void mouseLBDown( LPARAM& lParam );
    void mouseLBUp();

    void commandActions( WPARAM& wParam );

private:
    void initializeRect( LPARAM& lParam );

    void recreatePen();
    void setOpenFileParam();
    void mainWndAddMenus();
    void createFigures();
    void destroyFigures();

    Figure* findFigure( EFigureActions figureName );

    void commandForFigures( WPARAM& wParam );

    void clearBG();
    void saveIntoBg();

    //Selecting our pen with chosen color and thickness
    void setPen();

    void drawWhiteBgRec();

private:

    HPEN m_hPen = NULL; //Our Pen for color and size of brush

    COLORREF m_currentColor = 0x00000000;
    int m_currentThickness = 1;

    OPENFILENAMEA m_ofn; //For SaveAs action
    char m_fileName[260]; //File name path

private:

    HDC m_hdc;                       // handle to device context 
    RECT m_rcClient;                 // client area rectangle 
    POINT m_ptClientUL;              // client upper left corner 
    POINT m_ptClientLR;              // client lower right corner 
    POINTS m_ptsBegin;        // beginning point 
    POINTS m_ptsEnd;

    //For double buffering
    HDC m_memDC = NULL;
    HBITMAP m_bgBtmp = NULL;
    HBITMAP m_currBtmp = NULL;

    HDC m_bgDC = NULL;

private:

    std::unordered_map<EFigureActions, Figure*> m_figuresMap;

    Figure* m_pCurrentFigure;

};

#endif //_MAIN_WINDOW_HPP_