#ifndef _PEN_FIGURE_HPP_
#define _PEN_FIGURE_HPP_

#include "figure.h"

class PenFigure
	:public Figure
{
public:

    PenFigure( HDC &memDC, HDC& bgDC, HBITMAP& bgBtmp, RECT& rcClient )
        :m_memDC( memDC ), m_bgDC( bgDC ), m_bgBtmp( bgBtmp ), m_rcClient(rcClient)
    {}

    virtual void drawFigure( HDC& memDC, POINTS& ptsBegin, POINTS& ptsEnd, LPARAM& lParam ) override;

private:

    HDC& m_memDC;
    HDC& m_bgDC;

    HBITMAP& m_bgBtmp;

    RECT &m_rcClient;

private:

	void saveIntoBgPen();
    void clearBgPen();

};

#endif // _PEN_FIGURE_HPP_