#ifndef _LINE_FIGURE_HPP_
#define _LINE_FIGURE_HPP_

#include "figure.h"

class LineFigure
	:public Figure
{
public:

    virtual void drawFigure( HDC& memDC, POINTS& ptsBegin, POINTS& ptsEnd, LPARAM& lParam ) override;

};

#endif //_LINE_FIGURE_HPP_