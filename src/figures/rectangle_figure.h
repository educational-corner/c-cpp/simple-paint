#ifndef _RECTANGLE_FIGURE_HPP_
#define _RECTANGLE_FIGURE_HPP_

#include "figure.h"

class RectangleFigure
	:public Figure
{
public:

    virtual void drawFigure( HDC& memDC, POINTS& ptsBegin, POINTS& ptsEnd, LPARAM& lParam ) override;

};

#endif //_RECTANGLE_FIGURE_HPP_

