#ifndef _FIGURE_HPP_
#define _FIGURE_HPP_

#include <windows.h>

class Figure
{
public:

    virtual void drawFigure( HDC& memDC, POINTS& ptsBegin, POINTS& ptsEnd, LPARAM& lParam ) = 0;
};


#endif //_FIGURE_HPP_