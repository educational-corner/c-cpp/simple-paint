#include "rectangle_figure.h"

void RectangleFigure::drawFigure( HDC& memDC, POINTS& ptsBegin, POINTS& ptsEnd, LPARAM& lParam )
{
    Rectangle( memDC, ptsBegin.x, ptsBegin.y, ptsEnd.x, ptsEnd.y );
}