#include "pen_figure.h"

void PenFigure::drawFigure( HDC& memDC, POINTS& ptsBegin, POINTS& ptsEnd, LPARAM& lParam )
{
    clearBgPen();

    MoveToEx( memDC, ptsBegin.x, ptsBegin.y, NULL );
    LineTo( memDC,
        ptsBegin.x = LOWORD( lParam ),
        ptsBegin.y = HIWORD( lParam ));

    saveIntoBgPen();
}

void PenFigure::saveIntoBgPen()
{
    m_bgDC = CreateCompatibleDC( m_memDC );

    m_bgBtmp = CreateCompatibleBitmap( m_memDC,
                                        m_rcClient.right - m_rcClient.left,
                                        m_rcClient.bottom - m_rcClient.top );

    SelectObject( m_bgDC, m_bgBtmp );

    BitBlt( m_bgDC,
            0, 0,
            m_rcClient.right - m_rcClient.left,
            m_rcClient.bottom - m_rcClient.top,
            m_memDC,
            0, 0,
            SRCCOPY );
}

void PenFigure::clearBgPen()
{
    DeleteObject( m_bgBtmp );
    DeleteDC( m_bgDC );
}