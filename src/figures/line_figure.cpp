#include "line_figure.h"

void LineFigure::drawFigure( HDC& memDC, POINTS& ptsBegin, POINTS& ptsEnd, LPARAM& lParam )
{
    MoveToEx( memDC, ptsBegin.x, ptsBegin.y, ( LPPOINT )NULL );
    LineTo( memDC, ptsEnd.x, ptsEnd.y );
}