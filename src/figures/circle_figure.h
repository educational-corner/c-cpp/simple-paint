#ifndef _CIRCLE_FIGURE_HPP_
#define _CIRCLE_FIGURE_HPP_

#include "figure.h"

class CircleFigure
	:public Figure
{
public:

    virtual void drawFigure( HDC& memDC, POINTS& ptsBegin, POINTS& ptsEnd, LPARAM& lParam ) override;

};

#endif //_CIRCLE_FIGURE_HPP_

