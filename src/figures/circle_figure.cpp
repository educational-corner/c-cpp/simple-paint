#include "circle_figure.h"


void CircleFigure::drawFigure( HDC& memDC, POINTS& ptsBegin, POINTS& ptsEnd, LPARAM& lParam )
{
    Ellipse( memDC, ptsBegin.x, ptsBegin.y, ptsEnd.x, ptsEnd.y );
}