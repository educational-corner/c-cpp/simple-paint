#include "main_window.h"
#include "../res/Resource.h"
#include <typeinfo>
#include <cassert>

LRESULT MainWindow::HandleMessage( UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    switch ( uMsg )
    {
        case WM_CREATE:
        {
            mainWndAddMenus();
            setOpenFileParam();

            m_hPen = CreatePen( PS_SOLID, 1, NULL );

            createFigures();
        }
            break;
        case WM_COMMAND:
        {
            commandActions( wParam );
        }
        break;
        case WM_LBUTTONDOWN:
        {
            mouseLBDown( lParam );
        }
        break;
        case WM_LBUTTONUP:
        {
            mouseLBUp();
        }
            break;
        case WM_MOUSEMOVE:
        {
            mouseMove( wParam, lParam );
        }
            break;
        case WM_CLOSE:
            DestroyWindow( m_hwnd );
        case WM_DESTROY:
        {
            clearBG();
            destroyFigures();

            PostQuitMessage( 0 );
        }
            return 0;
        case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint( m_hwnd, &ps );

            FillRect( hdc, &ps.rcPaint, ( HBRUSH )( COLOR_WINDOW + 1 ));

            //Each time redrawing our BG image
            BitBlt( hdc, 0, 0, ps.rcPaint.right - ps.rcPaint.left , ps.rcPaint.bottom - ps.rcPaint.top , m_bgDC, 0, 0, SRCCOPY );

            EndPaint( m_hwnd, &ps );
        }
        break;

        default:
            return DefWindowProc( m_hwnd, uMsg, wParam, lParam );
    }
    return TRUE;
}

void MainWindow::mainWndAddMenus()
{
    HMENU RootMainMenu = CreateMenu();
    HMENU SubMainMenu = CreateMenu();
    HMENU SubShapesMenu = CreateMenu();
    HMENU SubThicknessMenu = CreateMenu();


    AppendMenu( RootMainMenu, MF_POPUP, ( UINT_PTR )SubMainMenu, L"File" );
    AppendMenu( SubMainMenu, MF_STRING, SAVE_AS_ACTION, L"Save as" );

    AppendMenu( RootMainMenu, MF_POPUP, ( UINT_PTR )SubThicknessMenu, L"Brush Thickness" );
    AppendMenu( SubThicknessMenu, MF_STRING, PEN_THICKNESS_01, L"1" );
    AppendMenu( SubThicknessMenu, MF_STRING, PEN_THICKNESS_05, L"5" );
    AppendMenu( SubThicknessMenu, MF_STRING, PEN_THICKNESS_10, L"10" );

    AppendMenu( RootMainMenu, MF_POPUP, COLOR_CHANGE, L"Change Color" );

    AppendMenu( RootMainMenu, MF_POPUP, ( UINT_PTR )SubShapesMenu, L"Drawing Shape" );
    AppendMenu( SubShapesMenu, MF_STRING, EFigureActions::eFA_PenAction, L"Pen" );
    AppendMenu( SubShapesMenu, MF_STRING, EFigureActions::eFA_LineAction, L"Line" );
    AppendMenu( SubShapesMenu, MF_STRING, EFigureActions::eFA_CircleAction, L"Circle" );
    AppendMenu( SubShapesMenu, MF_STRING, EFigureActions::eFA_RectangleAction, L"Rectangle" );

    SetMenu( m_hwnd, RootMainMenu );
   
}

void MainWindow::chColor()
{
    CHOOSECOLOR cc;                 // common dialog box structure 
    COLORREF acrCustClr[16]; // array of custom colors 
    DWORD rgbCurrent= 0x00000000;        // initial color selection

    // Initialize CHOOSECOLOR 
    ZeroMemory( &cc, sizeof( cc ) );
    cc.lStructSize = sizeof( cc );
    cc.hwndOwner = m_hwnd;
    cc.lpCustColors = ( LPDWORD )acrCustClr;
    cc.rgbResult = rgbCurrent;
    cc.Flags = CC_FULLOPEN | CC_RGBINIT;

    if( ChooseColor( &cc ) == TRUE )
    {
        m_currentColor = cc.rgbResult;
    }
}

void MainWindow::recreatePen(  )
{
    DeleteObject( m_hPen );
    m_hPen = CreatePen( PS_SOLID, m_currentThickness, m_currentColor );
}

void MainWindow::setOpenFileParam()
{
    ZeroMemory( &m_ofn, sizeof( m_ofn ) );
    m_ofn.lStructSize = sizeof( m_ofn );
    m_ofn.hwndOwner = m_hwnd;
    strcpy_s( m_fileName, "saved_image.bmp" );
    m_ofn.lpstrFile = m_fileName;
    m_ofn.nMaxFile = sizeof( m_fileName );
    m_ofn.nMaxFile = sizeof( m_fileName );
    m_ofn.lpstrFilter = "*.bmp";
    m_ofn.nFilterIndex = 1;
    m_ofn.lpstrFileTitle = NULL;
    m_ofn.nMaxFileTitle = 0;
    m_ofn.lpstrInitialDir = NULL;
    m_ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
}

void MainWindow::saveImage()
{
    HDC hdcWindow;
    HDC hdcMemDC = NULL;
    HBITMAP hbmScreen = NULL;
    BITMAP bmpScreen;
    char filename[40] = " ";
    hdcWindow = GetDC( m_hwnd );
    hdcMemDC = CreateCompatibleDC( hdcWindow );

    if( !hdcMemDC )
    {
        MessageBox( m_hwnd, L"CreateCompatibleDC has failed", L"Failed", MB_OK );
    }
    RECT rcClient;
    GetClientRect( m_hwnd, &rcClient );

    SetStretchBltMode( hdcWindow, HALFTONE );


    // Create a compatible bitmap from the Window DC
    hbmScreen = CreateCompatibleBitmap( hdcWindow, rcClient.right - rcClient.left, rcClient.bottom - rcClient.top );

    if( !hbmScreen )
    {
        MessageBox( m_hwnd, L"CreateCompatibleBitmap Failed", L"Failed", MB_OK );
    }
    SelectObject( hdcMemDC, hbmScreen );
    if( !BitBlt( hdcMemDC,
                0, 0,
                rcClient.right - rcClient.left, rcClient.bottom - rcClient.top,
                hdcWindow,
                0, 0,
                SRCCOPY ) )
    {
        MessageBox( m_hwnd, L"BitBlt has failed", L"Failed", MB_OK );
    }
    GetObject( hbmScreen, sizeof( BITMAP ), &bmpScreen );

    BITMAPFILEHEADER   bmfHeader;
    BITMAPINFOHEADER   bi;

    bi.biSize = sizeof( BITMAPINFOHEADER );
    bi.biWidth = bmpScreen.bmWidth;
    bi.biHeight = bmpScreen.bmHeight;
    bi.biPlanes = 1;
    bi.biBitCount = 32;
    bi.biCompression = BI_RGB;
    bi.biSizeImage = 0;
    bi.biXPelsPerMeter = 0;
    bi.biYPelsPerMeter = 0;
    bi.biClrUsed = 0;
    bi.biClrImportant = 0;

    DWORD dwBmpSize = ( ( bmpScreen.bmWidth * bi.biBitCount + 31 ) / 32 ) * 4 * bmpScreen.bmHeight;
    HANDLE hDIB = GlobalAlloc( GHND, dwBmpSize );
    char* lpbitmap = ( char* )GlobalLock( hDIB );
    GetDIBits( hdcWindow, hbmScreen, 0,
        ( UINT )bmpScreen.bmHeight,
        lpbitmap,
        ( BITMAPINFO* )&bi, DIB_RGB_COLORS );

    HANDLE hFile = CreateFileA( m_fileName,
                                GENERIC_WRITE,
                                0,
                                NULL,
                                CREATE_ALWAYS,
                                FILE_ATTRIBUTE_NORMAL, NULL );
    DWORD dwSizeofDIB = dwBmpSize + sizeof( BITMAPFILEHEADER ) + sizeof( BITMAPINFOHEADER );
    bmfHeader.bfOffBits = ( DWORD )sizeof( BITMAPFILEHEADER ) + ( DWORD )sizeof( BITMAPINFOHEADER );
    bmfHeader.bfSize = dwSizeofDIB;
    bmfHeader.bfType = 0x4D42; //BM   
    DWORD dwBytesWritten = 0;
    WriteFile( hFile, ( LPSTR )&bmfHeader, sizeof( BITMAPFILEHEADER ), &dwBytesWritten, NULL );
    WriteFile( hFile, ( LPSTR )&bi, sizeof( BITMAPINFOHEADER ), &dwBytesWritten, NULL );
    WriteFile( hFile, ( LPSTR )lpbitmap, dwBmpSize, &dwBytesWritten, NULL );
    GlobalUnlock( hDIB );
    GlobalFree( hDIB );
    CloseHandle( hFile );
    DeleteObject( hbmScreen );
    DeleteObject( hdcMemDC );
    ReleaseDC( m_hwnd, hdcWindow );
}

void MainWindow::mouseLBDown(LPARAM& lParam)
{
    initializeRect( lParam );

    //Creating 
    m_hdc = GetDC(m_hwnd);
    m_memDC = CreateCompatibleDC( m_hdc );

    m_currBtmp = CreateCompatibleBitmap( m_hdc,
                                        m_rcClient.right - m_rcClient.left,
                                        m_rcClient.bottom - m_rcClient.top );

    SelectObject( m_memDC, m_currBtmp );

    
}

void MainWindow::mouseMove( WPARAM& wParam, LPARAM& lParam )
{
    if( wParam & MK_LBUTTON )
    {
        const type_info& tB = typeid(*m_pCurrentFigure);

        m_ptsEnd = MAKEPOINTS( lParam );

        //Drawing background white rectangle
        drawWhiteBgRec();

        //Selecting our pen with chosen color and thickness
        setPen();
        
        //ifwe drew something before - put it as background
        if( m_bgBtmp )
        {
            BitBlt( m_memDC,
                    0, 0,
                    m_rcClient.right - m_rcClient.left,
                    m_rcClient.bottom - m_rcClient.top,
                    m_bgDC,
                    0, 0,
                    SRCCOPY );
        }

        m_pCurrentFigure->drawFigure( m_memDC, m_ptsBegin, m_ptsEnd, lParam );

        BitBlt( m_hdc,
                0, 0,
                m_rcClient.right - m_rcClient.left,
                m_rcClient.bottom - m_rcClient.top,
                m_memDC,
                0, 0,
                SRCCOPY );
    }
}

void MainWindow::mouseLBUp()
{
    ClipCursor(NULL);

    clearBG();
    saveIntoBg();

    DeleteObject( m_currBtmp ); // delete bitmap since it is no longer required
    DeleteDC( m_memDC );
    ReleaseDC( m_hwnd, m_hdc );

    ReleaseCapture();
}

void MainWindow::initializeRect(  LPARAM& lParam  )
{
    SetCapture( m_hwnd );

    GetClientRect( m_hwnd, &m_rcClient );
    m_ptClientUL.x = m_rcClient.left;
    m_ptClientUL.y = m_rcClient.top;

    m_ptClientLR.x = m_rcClient.right;
    m_ptClientLR.y = m_rcClient.bottom;
    ClientToScreen( m_hwnd, &m_ptClientUL );
    ClientToScreen( m_hwnd, &m_ptClientLR );

    SetRect( &m_rcClient, m_ptClientUL.x, m_ptClientUL.y, m_ptClientLR.x, m_ptClientLR.y );

    ClipCursor( &m_rcClient );

    m_ptsBegin = MAKEPOINTS( lParam );
}

void MainWindow::clearBG()
{
    DeleteObject( m_bgBtmp );
    DeleteDC( m_bgDC );
}

void MainWindow::saveIntoBg()
{
    m_bgDC = CreateCompatibleDC( m_hdc );

    m_bgBtmp = CreateCompatibleBitmap( m_hdc,
                                        m_rcClient.right - m_rcClient.left,
                                        m_rcClient.bottom - m_rcClient.top );

    SelectObject( m_bgDC, m_bgBtmp );

    BitBlt( m_bgDC,
            0, 0,
            m_rcClient.right - m_rcClient.left,
            m_rcClient.bottom - m_rcClient.top,
            m_hdc,
            0, 0,
            SRCCOPY );
}

void MainWindow::setPen()
{
    SelectObject( m_memDC, m_hPen );
    SetROP2( m_memDC, R2_MASKPEN );
}

void MainWindow::drawWhiteBgRec()
{
    RECT rect;
    GetClientRect( m_hwnd, &rect );

    FillRect( m_memDC, &rect, ( HBRUSH )( COLOR_WINDOW + 1 ));
}

void MainWindow::commandActions( WPARAM& wParam )
{
    switch ( wParam )
    {
    case SAVE_AS_ACTION:
    {
        if(GetSaveFileNameA( &m_ofn ))
        {
            saveImage();
        }
    }
    break;
    case COLOR_CHANGE:
    {
        chColor();
        recreatePen();
    }
    break;
    case PEN_THICKNESS_01:
    {
        m_currentThickness = 1;
        recreatePen();
    }
    break;
    case PEN_THICKNESS_05:
    {
        m_currentThickness = 5;
        recreatePen();
    }
    break;
    case PEN_THICKNESS_10:
    {
        m_currentThickness = 10;
        recreatePen();
    }
    break;
    default:
        break;
    }

    commandForFigures( wParam );
}

void MainWindow::createFigures()
{
    Figure* m_pen = new PenFigure( m_memDC, m_bgDC, m_bgBtmp, m_rcClient );
    Figure* m_line = new LineFigure;
    Figure* m_circle = new CircleFigure;
    Figure* m_rectangle = new RectangleFigure;

    m_figuresMap[EFigureActions::eFA_PenAction] = m_pen;
    m_figuresMap[EFigureActions::eFA_LineAction] = m_line;
    m_figuresMap[EFigureActions::eFA_CircleAction] = m_circle;
    m_figuresMap[EFigureActions::eFA_RectangleAction] = m_rectangle;

    m_pCurrentFigure = m_pen;
}

void MainWindow::destroyFigures()
{
    auto itFigures = m_figuresMap.begin();

    while ( itFigures != m_figuresMap.end())
    {
        delete itFigures->second;
        itFigures->second = nullptr;
        ++itFigures;
    }
}

Figure* MainWindow::findFigure( EFigureActions figureName )
{
    auto figureIt = m_figuresMap.find( figureName );

    if( figureIt == m_figuresMap.end() )
    {
        return m_pCurrentFigure;
    }
    return figureIt->second;
}

void MainWindow::commandForFigures( WPARAM& wParam )
{
    Figure* pFigure = m_figuresMap[( EFigureActions )wParam];

    if( pFigure )
    {
        m_pCurrentFigure = pFigure;
    }
}